import os
import sys
import subprocess

os.system(f"echo Home is: $HOME")
os.system("rm -r $HOME/.pip")
os.system("mkdir $HOME/.pip")
os.system("echo '[global]' >> $HOME/.pip/pip.conf")
os.system(
    "echo 'extra-index-url=https://:idmwzesk7psaxuhwypvhlscecxbmyozg44ndkowlrkwifk3tjh4a@pkgs.dev.azure.com/innovyze/global/_packaging/app_emagin360/pypi/simple/' >> $HOME/.pip/pip.conf")
subprocess.check_call(
    [
        sys.executable,
        "-m",
        "pip",
        "install",
        "emagin-forecast==1.0.29"
    ]
)

# First install lib in container before attempting to import modules
# FIXME: Need to use a better way to import the required scripts for container
import pickle
import argparse
import importlib
import ast
import joblib
import pandas as pd
import copy
from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestRegressor

from emagin_forecast.recipe_schema.algorithm.scripts.train_split_method import train_model, target_transform
from emagin_forecast.split_methods import ts_kfold_split, calc_model_fit_results, SplitTrain



training_message = "training model"
mape_message = 'mape='
rmse_message = 'rmse='


class UnknownHyperParameter(Exception):
    """
    Exception when the hyperParameter is not known
    """
    pass


def to_number(s):
    try:
        s = float(s)
        if float(s).is_integer():
            return int(s)
        else:
            return float(s)
    except ValueError:
        return s


def prepare_train_test_df(train, train_file, test, test_file, split_method='sequence_split'):
    train_df = pd.read_csv(os.path.join(train, train_file), parse_dates=True, index_col='DATE_TIME')
    test_df = pd.read_csv(os.path.join(test, test_file), parse_dates=True, index_col='DATE_TIME')
    if split_method == 'backtest_split' or split_method == 'ts_kfold_split':
        train_df = train_df.append(test_df)
    return train_df, test_df


def get_model(module, model_class, hyperparameters):
    module = importlib.import_module(module)
    class_ = getattr(module, model_class)
    if all(k in class_().get_params() for k in hyperparameters.keys()):
        model = class_(**hyperparameters)
        return model
    else:
        raise UnknownHyperParameter("hyperparameter not recognized!")


def persist(saving_model, saving_format, model_dir):
    if saving_format == "pickle":
        ## Saving as pickle
        path = os.path.join(model_dir, "model.pkl")
        pickle.dump(saving_model, open(path, "wb"))
    elif saving_format == "joblib":
        ## Saving as Joblib
        path = os.path.join(model_dir, "model.joblib")
        joblib.dump(saving_model, path)
    msg = "model persisted at " + path
    print(msg)
    return msg


if __name__ == "__main__":
    print("extracting arguments")
    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.
    # to simplify the demo we don't use all sklearn RandomForest hyperparameters
    parser.add_argument("--hyperparameters", type=str)
    parser.add_argument("--forecast_type", type=str, default='regression')
    parser.add_argument("--module_package", type=str)
    parser.add_argument("--module_name", type=str)
    # Data, model, and output directories
    # All Args are sent as string
    parser.add_argument("--model-dir", type=str, default=os.environ.get("SM_MODEL_DIR"))
    parser.add_argument("--train", type=str, default=os.environ.get("SM_CHANNEL_TRAIN"))
    parser.add_argument("--test", type=str, default=os.environ.get("SM_CHANNEL_TEST"))
    parser.add_argument('--output-data-dir', type=str, default=os.environ.get('SM_OUTPUT_DATA_DIR'))
    parser.add_argument("--train-file", type=str, default="train.csv")
    parser.add_argument("--test-file", type=str, default="test.csv")

    parser.add_argument("--metrics", type=str, default="mape rmse")
    parser.add_argument("--saving_format", type=str, default="pickle")
    parser.add_argument("--split_method", type=str, default="splittesting")
    parser.add_argument("--target_transformation", type=str, default="{}")
    # Saving Model Format, the default is pickle

    parser.add_argument("--features", type=str)
    parser.add_argument("--target", type=str)

    args, _ = parser.parse_known_args()
    hp_parser = argparse.ArgumentParser()
    for name in eval(args.hyperparameters):
        hp_parser.add_argument(f"--{name}")
    hp_args, _ = hp_parser.parse_known_args()
    hyperparameters = hp_args.__dict__
    for key, value in hyperparameters.items():
        hyperparameters[key] = to_number(value)

    features_list = args.features.split()
    print("feature_cols: ", features_list)
    if args.target.startswith("[") and args.target.endswith("]"):
        target_cols = eval(args.target)
    else:
        target_cols = list(args.target.split(" "))
    print("target_cols: ", target_cols)

    a = args.target_transformation
    if len(a.split("=")) > 1:
        target_config = ast.literal_eval(str({a.split("=")[0]: ast.literal_eval(a.split("=")[1])}))
    else:
        target_config = {}

    print("reading data")
    train_df, test_df = prepare_train_test_df(args.train, args.train_file, args.test, args.test_file, args.split_method)
    train_df = target_transform(train_df, target_cols, target_config)

    if args.split_method == 'backtest_split':
        eval_parser = argparse.ArgumentParser()
        eval_parser.add_argument(f"--n_splits", type=int)
        eval_args, _ = eval_parser.parse_known_args()
        n_splits = eval_args.n_splits
    elif args.split_method == 'ts_kfold_split':
        eval_parser = argparse.ArgumentParser()
        eval_parser.add_argument(f"--k_folds", type=int, default=5)
        eval_parser.add_argument(f"--ranges_per_fold", type=int, default=1)
        eval_parser.add_argument(f"--lag_filter", type=int, default=0)
        eval_parser.add_argument(f"--train_portion_min", type=float, default=0.9)
        eval_parser.add_argument(f"--test_splits", type=int, default=None)
        eval_args, _ = eval_parser.parse_known_args()

        ts_kfold_split_config = {
            'ts_kfold_split': {
                'k_folds':eval_args.k_folds,
                'ranges_per_fold':eval_args.ranges_per_fold,
                'lag_filter':eval_args.lag_filter,
                'train_portion_min':eval_args.train_portion_min,
                'test_splits':eval_args.test_splits,
            }
        }
    else:
        n_splits = None
    # train
    print(training_message)

    # Get desired model
    pretrain_model = get_model(args.module_package, args.module_name, hyperparameters)

    # Model Training:
    if args.split_method in ['sequence_split', 'backtest_split']:
        trained_model = train_model(pretrain_model, args.module_name, args.split_method, train_df, test_df,
                                    args.metrics, target_cols, features_list, n_splits, args.forecast_type, target_config)

    elif args.split_method == 'ts_kfold_split':
        cv_trainer = SplitTrain(train_df, ts_kfold_split_config)
        cv_trainer.split()

        instance_estimator = RandomForestRegressor(n_jobs=1, **hyperparameters)
        cv_trainer.cv_predict(features_list, target_cols, instance_estimator)
        model_results = cv_trainer.calc_model_results()
        print('mape_cv=' + str(model_results['mape_cv']) + ';', file=sys.stdout)
        print('r2_cv=' + str(model_results['r2_cv']) + ';', file=sys.stdout)
        print('rmse_cv=' + str(model_results['rmse_cv']) + ';', file=sys.stdout)
        print('mse_cv=' + str(model_results['mse_cv']) + ';', file=sys.stdout)
        print('qt_0.25_cv=' + str(model_results['qt_0.25_cv']) + ';', file=sys.stdout)
        print('qt_0.5_cv=' + str(model_results['qt_0.5_cv']) + ';', file=sys.stdout)
        print('qt_0.75_cv=' + str(model_results['qt_0.75_cv']) + ';', file=sys.stdout)
        print('qt_0.95_cv=' + str(model_results['qt_0.95_cv']) + ';', file=sys.stdout)
        print('qt_0.99_cv=' + str(model_results['qt_0.99_cv']) + ';', file=sys.stdout)

        print('mape_is=' + str(model_results['mape_is']) + ';', file=sys.stdout)
        print('r2_is=' + str(model_results['r2_is']) + ';', file=sys.stdout)
        print('rmse_is=' + str(model_results['rmse_is']) + ';', file=sys.stdout)
        print('mse_is=' + str(model_results['mse_is']) + ';', file=sys.stdout)
        print('qt_0.25_is=' + str(model_results['qt_0.25_is']) + ';', file=sys.stdout)
        print('qt_0.5_is=' + str(model_results['qt_0.5_is']) + ';', file=sys.stdout)
        print('qt_0.75_is=' + str(model_results['qt_0.75_is']) + ';', file=sys.stdout)
        print('qt_0.95_is=' + str(model_results['qt_0.95_is']) + ';', file=sys.stdout)
        print('qt_0.99_is=' + str(model_results['qt_0.99_is']) + ';', file=sys.stdout)
        
        trained_model = copy.deepcopy(instance_estimator)
        trained_model.fit(train_df[features_list], train_df[target_cols])

    else:
        raise Exception('Unknown splitting method!!')

    ##### persist model
    persist(trained_model, args.saving_format, args.model_dir)
